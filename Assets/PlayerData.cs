using System;
using System.Collections.Generic;
using System.Linq;

public static class MathUtilites
{

    public static float ClosestTo(this IEnumerable<float> collection, float target)
    {
        if (collection.Any())
        {
            return collection.OrderBy(x => Math.Abs(target - x)).First();

        }
        else
        {
            return 0;
        }
    }
}
