using System;
using UniMob;
using UnityEngine.UI;
using UnityEngine;
using Random = UnityEngine.Random;

public class SampleCounter : LifetimeMonoBehaviour
{
    public Text counterText;
    public Button incrementButton;

    // declare reactive property
    [Atom] private int Counter { get; set; }

    public GameDataforPlayer data;

    protected override void Start()
    {
        // increment Counter on button click
        incrementButton.onClick.AddListener(() => LevelUp());

        // Update counterText when Counter changed until Lifetime terminated
        Atom.Reaction(Lifetime, () => counterText.text ="Speed: " +data.moveSpeed+ "\nRadius: " + data.range + "\nDamage: " +
        data.damage + "\nKills " + data.kills + "\nLevel " + data.levl);


    }

    public void LevelUp()
    {
        data.levl++;
        int rnadNum = Random.Range(0, 101);
        if(rnadNum <= 10)
        {
            data.range++;
        }else if(rnadNum >= 11 && rnadNum <= 40)
        {
            data.damage+=10;

        }
        else
        {
            data.moveSpeed += 0.5f;

        }

        //Debug.Log("Random: " + rnadNum);
    }

}
