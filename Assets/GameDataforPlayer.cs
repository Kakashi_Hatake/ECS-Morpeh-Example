using System.Collections;
using System.Collections.Generic;
using UniMob;
using UnityEngine;

public class GameDataforPlayer : LifetimeMonoBehaviour
{
    [Atom]    public int range {get; set;}
    [Atom] public float moveSpeed { get; set; }
    [Atom] public int kills { get; set; }
    [Atom] public int damage { get; set; }
    [Atom] public int levl { get; set; }

    public int rangePub;
    public int moveSpeedPub;
    public int damagePub;


    private void Awake()
    {
        range = rangePub;
        moveSpeed = moveSpeedPub;
        damage = damagePub;
    }

}
