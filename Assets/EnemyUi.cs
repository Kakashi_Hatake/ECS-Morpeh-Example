using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyUi : MonoBehaviour
{
    public Slider slider;

    public void SetSliderValue(int hela, int maxHealth)
    {
        slider.maxValue = maxHealth;
        slider.value = hela;
    }
}
