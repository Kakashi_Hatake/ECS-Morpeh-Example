using System;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using UnityEngine;

[Serializable]

public struct MoveComp : IComponent, IValidatableWithGameObject
{
    public CharacterController controller;
    public Vector3 moveVector;
    public Transform playerPos;
    public GameDataforPlayer data;

    public void OnValidate(GameObject gameObject)
    {
        if (controller == null)
        {
            controller = gameObject.GetComponent<CharacterController>();
        }
        if (data == null)
        {
            data = gameObject.GetComponent<GameDataforPlayer>();
            Debug.LogError("GameDataforPlayer" + data.range);

        }
    }
}

public class PlayerMove : MonoProvider<MoveComp> { }
