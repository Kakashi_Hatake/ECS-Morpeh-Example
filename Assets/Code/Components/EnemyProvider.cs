 using System;
    using Scellecs.Morpeh;
    using Scellecs.Morpeh.Providers;
    using UnityEngine;

public class EnemyProvider : MonoProvider<EnemyDamage> { }

[Serializable]
public struct EnemyDamage : IComponent, IValidatableWithGameObject
{
    public int health;
    public int maxHealth;
    public Vector3 m_Target;
    public CharacterController controller;
    public EnemyUi ui;
    public EnemyData enemydata;
    public void OnValidate(GameObject gameObject)
    {
        if (controller == null)
        {
            controller = gameObject.GetComponent<CharacterController>();
        }

        if(ui == null)
        {
            ui = gameObject.GetComponent<EnemyUi>();
        }

        if(enemydata == null)
        {
            enemydata = gameObject.GetComponent<EnemyData>();
        }
    }


}