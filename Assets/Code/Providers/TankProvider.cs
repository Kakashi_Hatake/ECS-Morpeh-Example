using System;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using Unity.VisualScripting;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class TankProvider : MonoProvider<Tank> { }

[field: SerializeField]
[Inspectable]
public struct Tank : IComponent
{
    public Rigidbody2D body;
    public Vector2 userTextOffset;
}