using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(DeathEventSystem))]
public sealed class DeathEventSystem : UpdateSystem {
    private Filter eventSpawn;
    private Filter player;

    public override void OnAwake() {
        eventSpawn = World.Filter.With<DeathEvent>();
        player = World.Filter.With<MoveComp>();
        GameObject enemy = Instantiate(Resources.Load<GameObject>("Enemy_" + (Random.Range(1, 4).ToString())), new Vector3(Random.Range(-10, 10), 3, Random.Range(-10, 10)), Quaternion.identity);

    }

    public override void OnUpdate(float deltaTime) {
        foreach (Entity entity in eventSpawn)
        {
            ref var eventLocal = ref entity.GetComponent<DeathEvent>();
            //Debug.LogError("Killed");

            GameObject enemy = Instantiate(Resources.Load<GameObject>("Enemy_"+ (Random.Range(1, 4).ToString())), new Vector3(Random.Range(-10, 10),3, Random.Range(-10, 10)), Quaternion.identity);
            entity.RemoveComponent<DeathEvent>();
            foreach (Entity ent2 in player)
            {
                ref var player = ref ent2.GetComponent<MoveComp>();
                player.data.kills++;
            }
        }



    }
}