using Scellecs.Morpeh.Systems;
using UnityEngine;
using Unity.IL2CPP.CompilerServices;
using Scellecs.Morpeh;
using System.Collections.Generic;
using Unity.VisualScripting;
using Random = UnityEngine.Random;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(EnemyDamageSystem))]
public sealed class EnemyDamageSystem : UpdateSystem
{
    private Filter filter;
    private bool isAttacking;
    private float timer;
    private Filter player;

    public override void OnAwake()
    {
        this.filter = this.World.Filter.With<EnemyDamage>();
        this.player = this.World.Filter.With<MoveComp>();

        for(int i = 0; i < 5; i++)
        {
            GameObject enemy = Instantiate(Resources.Load<GameObject>("Enemy_" + (Random.Range(1, 4).ToString())), new Vector3(Random.Range(-10, 10), 3, Random.Range(-10, 10)), Quaternion.identity);
        }

    }

    public override void OnUpdate(float deltaTime)
    {
        int range = 0;
        int damage = 0;
        List<EnemyDamage> EnemyDamages = new List<EnemyDamage>();
        List<float> enemyDistance = new List<float>();
        if (!isAttacking)
        {
            Vector3 playerPos = new Vector3();
            foreach (var entity in this.player)
            {
                ref var playerComp = ref entity.GetComponent<MoveComp>();
                playerPos = playerComp.playerPos.position;
                range = playerComp.data.range;
                damage = playerComp.data.damage;
            }

            foreach (var entity in this.filter)
            {
                ref var enemy = ref entity.GetComponent<EnemyDamage>();
                enemy.m_Target = enemy.controller.transform.position;
                float dist = Vector3.Distance(playerPos, enemy.m_Target);
                //Debug.LogError("playerPos" + playerPos + " enemyPos " + enemy.m_Target);

                if (enemy.enemydata.enemyHeal < 0)
                {
                    entity.SetComponent(new IsDeadMarker());
                    //Debug.LogError("IsDeadMarker");
                }

                //float dist = 2;
                // Debug.LogError(dist+ "  <=" + PlayerData.radius);
                if (dist <= range && !isAttacking)
                {
                    EnemyDamages.Add(enemy);
                    enemyDistance.Add(dist);

                }

            }
            if(!(enemyDistance.Count > 2))
            {
                for (int i = 0; i < EnemyDamages.Count; i++)
                {
                    timer = 1;
                    Debug.Log("Attack");
                    EnemyDamages[i].enemydata.enemyHeal -= damage;
                    EnemyDamages[i].ui.SetSliderValue(EnemyDamages[i].enemydata.enemyHeal, EnemyDamages[i].enemydata.enemyMax);
                }
            }
            else
            {
                for (int i = 0; i < 3; i++)
                {
                    float closet = MathUtilites.ClosestTo(enemyDistance, 0);

                    for (int damgI = 0; damgI < enemyDistance.Count; damgI++)
                    {
                        if (closet == enemyDistance[damgI])
                        {
                            Debug.Log("Attack");
                            EnemyDamages[damgI].enemydata.enemyHeal -= damage;
                            EnemyDamages[damgI].ui.SetSliderValue(EnemyDamages[damgI].enemydata.enemyHeal, EnemyDamages[damgI].enemydata.enemyMax);
                            enemyDistance.RemoveAt(damgI);
                            EnemyDamages.RemoveAt(damgI);



                        }
                    }

                }
                timer = 1;
                isAttacking = true;

            }


            if (EnemyDamages.Count > 0)
            {
                isAttacking = true;

            }

            //Debug.Log(isAttacking + " " + timer);
           
        }

        if (Time.time >= timer && isAttacking)
        {
            //Debug.Log(Time.time + ">=" + timer);

            timer = 0;
            isAttacking = false;
        }

    }


}