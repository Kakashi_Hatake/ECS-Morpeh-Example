using Scellecs.Morpeh.Systems;
using UnityEngine;
using Unity.IL2CPP.CompilerServices;
using Scellecs.Morpeh;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(EnemyDestroySystem))]
public sealed class EnemyDestroySystem : UpdateSystem {
    private Filter destroyEnemy;

    public override void OnAwake() {
        destroyEnemy = World.Filter.With<EnemyDamage>().With<IsDeadMarker>();
        //destroyEnemy = World.Filter.With<IsDeadMarker>();

    }

    public override void OnUpdate(float deltaTime) {
        foreach (Entity ent in destroyEnemy)
        {
            var entity = this.World.CreateEntity();
            ref var dethMessange = ref entity.AddComponent<DeathEvent>();


            GameObject enemyGo = ent.GetComponent<EnemyDamage>().controller.gameObject;
            World.RemoveEntity(ent);
            Destroy(enemyGo);
            //Debug.LogError("Destroy");
        }
    }



}