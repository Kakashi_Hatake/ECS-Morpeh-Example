using Scellecs.Morpeh.Systems;
using UnityEngine;
using Unity.IL2CPP.CompilerServices;
using Scellecs.Morpeh;
using static UnityEngine.EventSystems.EventTrigger;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(InputSystem
    ))]
public sealed class InputSystem : UpdateSystem {
    private Filter filter;

    public override void OnAwake() {
        this.filter = this.World.Filter.With<MoveComp>();
        //  this.followComp = this.World.GetStash<Follow>();




    }

    public override void OnUpdate(float deltaTime) {
        foreach (var entity in this.filter)
        {
            ref var Movable = ref entity.GetComponent<MoveComp>();
            Movable.moveVector =  new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            //Debug.Log(Movable.moveVector);
            Movable.controller.Move(Movable.moveVector * Time.deltaTime * Movable.data.moveSpeed);
            Movable.playerPos = Movable.controller.gameObject.transform;

        }


    }
}