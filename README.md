Simple ECS Morpeh demo project.

Game logic implemented on Morpeh ECS;
The hero data model is separated from the simulation ECS and from the user interface;
The user interface is updated reactively (UniMob).


The player controls the hero. Moving the hero across the playing field carried out using the arrow keys on the keyboard. On the playing field Enemies are located at random points. When the hero approaches the enemy closer than at attack distance, the hero begins to inflict continuous damage to the enemy. The hero can attack no more than three targets at the same time. If enemies
more than the maximum number of targets, nearby enemies will be attacked. After destruction of the enemy on the playing field at a random point, a new enemy appears. Enemies must be of different types. Each type of enemy has a prefab and health indicator. The hero is given a prefab, a speed indicator movement, damage per second, attack radius. The UI should be located a counter of destroyed enemies, all player indicators and one pumping button.
When you press the boost button, one random indicator increases
hero (for each pumping, the probability of its improvement in
percent). All characteristics and constants must be set via
inspector.
